
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <ctype.h>
#include <signal.h>

void launch(char *args);

void dirChange(char *input)
{
    printf("dir change ");
    if (strcmp(input,"")==0)
    {

        char *home = getenv("HOME");
        strcat(home, "/");
        chdir(home);
    }
    else
    {

        if (chdir(input) != 0)
        {
           perror("cd:" );
        }
    }
}


void split(char *userInput, char **arguments, int *count)
{
    int i = 0;
    char *temp = strtok(userInput, " ");
    arguments = realloc(arguments, 1024 * sizeof(char *));

    while (temp != NULL)
    {
        arguments[i] = temp;
        i++;
        arguments = realloc(arguments, 1024 * sizeof(char *));
        temp = strtok(NULL, " ");
    }

    *count = i;
}


int main(void){
    printf("start \n");
    printf("yeah \n");
    dirChange("");
    printf("lol \n");
    printf("System \n");
    system("gcc /home/mikhail/Desktop/smarthome/TermometerReading.c -o reader");
    printf("1st \n");
    launch("node-red -p 1880");
    launch("./reader");
    launch("node Desktop/smarthome/TermometerServer.js");


    return 1;
}


void launch(char *args)
{
    pid_t pid = fork();
    int status;

    if (pid == -1)
        printf("Forking failed\n");
    else if (pid == 0)
        system(args);
}
